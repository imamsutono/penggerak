@extends('layouts.app')

@section('title', 'Farmer')

@section('css_source')
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style media="screen">
      .slick-arrow > i.fa {
        margin-top: 8px !important;
        cursor: pointer;
      }
      a:hover {text-decoration: none;}
    </style>
@endsection

@section('body')
  <main>
    <div class="container">
      <div class="separator"></div>
      <h1 class="text-center">Petani Bibit Konservasi dan Penggerak LindungiHutan.com</h1>
      <p class="text-center">Sedikit masyarakat yang sadar pentingnya penghijauan untuk kepentingan alam, masa depan anak cucu kita dan kelestarian Alam Hutan Indonesia. Penggerak dan petani di LindungiHutan memiliki peran penting, mengesampingkan kepentingan pribadi untuk kelestarian Alam. Mereka melakukan pembibitan tanaman konservasi yang notabene "Tidak Laku" di pasaran kalah bersaing dengan tanaman produktif, kelapa sawit, dan sayuran. Kita harus mendukung mereka agar tetap bisa melanjutkan perjuangan melestarikan Alam dan Hutan. Bersama Menghijaukan Indonesia dengan platform LindungiHutan kita mulai peduli dan berkontribusi untuk kelestarian Alam.</p>
      <div class="separator"></div>

      <div class="row farmer">
        <div class="col-sm-4 col-xs-12">
          <a href="{{ url('penggerak/detail') }}">
            <div class="card">
              <img src="img/profile.png" alt="Petani Penggerak - LindungiHutan">
              <div class="card-content clr-bamboo">
                <p class="card-title">Pak Faris Setia</p>
                <p class="text-center">Pasuruan</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-4 col-xs-12">
          <a href="{{ url('penggerak/detail') }}">
            <div class="card">
              <img src="img/profile.png" alt="Petani Penggerak - LindungiHutan">
              <div class="card-content clr-bamboo">
                <p class="card-title">Pak Faris Setia</p>
                <p class="text-center">Pasuruan</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-4 col-xs-12">
          <a href="{{ url('penggerak/detail') }}">
            <div class="card">
              <img src="img/profile.png" alt="Petani Penggerak - LindungiHutan">
              <div class="card-content clr-bamboo">
                <p class="card-title">Pak Faris Setia</p>
                <p class="text-center">Pasuruan</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-4 col-xs-12">
          <a href="{{ url('penggerak/detail') }}">
            <div class="card">
              <img src="img/profile.png" alt="Petani Penggerak - LindungiHutan">
              <div class="card-content clr-bamboo">
                <p class="card-title">Pak Faris Setia</p>
                <p class="text-center">Pasuruan</p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="separator"></div>

      <button type="button" class="btn-lh btn-bamboo center">Jadi Penggerak</button>
      <div class="separator"></div>
    </div>
    <div class="bg-aglonema padding-v-lg text-center">
      <div class="container">
        <h1>Apa yang bisa kamu lakukan di LindungiHutan?</h1>
        <div class="separator-sm"></div>

        <div class="row">
          <div class="col-sm-4">
            <img src="img/icons/pendukung.png" alt="Pendukung Alam Raya - LindungiHutan" class="center-block">
            <h3><strong>Pendukung</strong></h3>
            <strong>Pendukung</strong> dapat <strong>Berdonasi</strong> dan <strong>Gabung Aksi</strong> di Kampanye Alam, Pendukung juga dapat melihat <strong>Update Pohon</strong> melalui Pantau Alam atau bergabung Menjadi Relawan.
          </div>
          <div class="col-sm-4">
            <img src="img/icons/penggalang.png" alt="Penggalang Alam Raya - LindungiHutan" class="center-block">
            <h3><strong>Penggalang</strong></h3>
            <strong>Penggalang</strong> adalah Pribadi/Kelompok yang membuat <strong>Kampanye Alam</strong> untuk menggalang dana membantu Penggerak di Wilayah Konservasi.
          </div>
          <div class="col-sm-4">
            <img src="img/icons/penggerak.png" alt="Penggerak Alam Raya - LindungiHutan" class="center-block">
            <h3><strong>Penggerak</strong></h3>
            <strong>Penggerak</strong> adalah Pribadi/Kelompok pengelola <strong>Kawasan Konservasi</strong> atau yang melakukan kegiatan dengan isu Lingkungan
          </div>
        </div>
      </div>
    </div>
    <div class="separator"></div>

    <img src="img/chat.png" class="chat-icon" alt="Chat - HousePlantTree - LindungiHutan">
  </main>
@endsection

@section('js_source')
    <script src="{{ asset('js/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/imam.js') }}"></script>
@endsection
