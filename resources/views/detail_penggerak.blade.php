@extends('layouts.app')

@section('title', 'Andreas Aji Wibowo - Penggerak')

@section('css_source')
  <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('body')
    <main>
      <!-- Farmer profile -->
      <div id="farmer-profile">
        <div class="container">
          <div class="row">
            <div class="col-md-2 col-xs-4">
              <img src="{{ asset('img/profile.png') }}" class="img-rounded" width="100%" alt="Petani Penggerak - LindungiHutan">
            </div>
            <div class="col-md-5 col-xs-8">
              <div class="font-md">Andreas Aji Wibowo</div>
              Kabupaten / Kota Cilacap<br><br>
              <small class="hidden-xs">
                <i>Saya adalah mahasiswa yang ingin menghijaukan kawasan pesisir Cilacap. Mari kita bersama menjaga kelestarian hutan di daerah pesisir.</i>
              </small>
            </div>
            <div class="col-md-1 hidden-xs"></div>
            <div class="col-md-4 col-xs-12">
              <div class="card bg-bamboo clr-snowdrop">
                <div class="card-content">
                  <div class="row">
                    <div class="col-xs-6 border-right-snowdrop">
                      <small>Pohon Tertanam</small>
                      <p class="no-margin font-md">2000 Pohon</p>
                    </div>
                    <div class="col-xs-6">
                      <small>Karbon Terserap</small>
                      <p class="no-margin font-md">2000 KG</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End farmer profile -->
      <div class="separator"></div>
      <!-- Documentation -->
      <div class="container">
        <h3><strong>Dokumentasi Penanaman</strong></h3>

        <div id="documentation">
          <img src="{{ asset('img/campaign.png') }}" alt="Petani Penggerak - LindungiHutan">
          <img src="{{ asset('img/campaign.png') }}" alt="Petani Penggerak - LindungiHutan">
          <img src="{{ asset('img/campaign.png') }}" alt="Petani Penggerak - LindungiHutan">
          <img src="{{ asset('img/campaign.png') }}" alt="Petani Penggerak - LindungiHutan">
          <img src="{{ asset('img/campaign.png') }}" alt="Petani Penggerak - LindungiHutan">
        </div>
      </div>
      <!-- End documentation -->

      <!-- Planting area -->
      <div id="planting-area" class="container">
        <div class="separator"></div>
        <h3><strong>Wilayah Penanaman</strong></h3>

        <div class="card">
          <div class="row padding-lg">
            <div class="col-md-2 col-xs-3">
              <img src="{{ asset('img/planting.png') }}" width="100%" alt="Wilayah Penanaman - LindungiHutan">
            </div>
            <div class="col-md-3 col-md-offset-1 hidden-xs">
              <strong>Tanggal Penanaman</strong><br>
              20 Desember 2020
              <div class="separator"></div>
              <strong>Lokasi</strong><br>
              Kampung Laut, Cilacap
            </div>
            <div class="col-md-3 hidden-xs">
              <strong>Kab/Kota</strong><br>
              Cilacap
              <div class="separator"></div>
              <strong>Jumlah</strong><br>
              2,936 Pohon dari<br>
              5 Kampanye Alam
            </div>
            <div class="col-xs-9 hidden-sm hidden-md hidden-lg hidden-xl">
              <strong>Kampung Laut Cilacap</strong><br>
              20 Desember 2020
            </div>
            <div class="separator-sm hidden-sm hidden-md hidden-lg hidden-xl"></div>
            <div class="col-md-3">
              <div class="row">
                <div class="col-md-12 col-xs-6">
                  <button type="button" class="btn-lh btn-block btn-bamboo">Buat Kampanye</button>
                </div>
                <div class="col-md-12 col-xs-6">
                  <a href="{{ url('penggerak/lokasi') }}">
                    <button type="button" class="btn-lh btn-block btn-bamboo-outline">Lihat Lokasi</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="row padding-lg">
            <div class="col-md-2 col-xs-3">
              <img src="{{ asset('img/planting.png') }}" width="100%" alt="Wilayah Penanaman - LindungiHutan">
            </div>
            <div class="col-md-3 col-md-offset-1 hidden-xs">
              <strong>Tanggal Penanaman</strong><br>
              20 Desember 2020
              <div class="separator"></div>
              <strong>Lokasi</strong><br>
              Kampung Laut, Cilacap
            </div>
            <div class="col-md-3 hidden-xs">
              <strong>Kab/Kota</strong><br>
              Cilacap
              <div class="separator"></div>
              <strong>Jumlah</strong><br>
              2,936 Pohon dari<br>
              5 Kampanye Alam
            </div>
            <div class="col-xs-9 hidden-sm hidden-md hidden-lg hidden-xl">
              <strong>Kampung Laut Cilacap</strong><br>
              20 Desember 2020
            </div>
            <div class="separator-sm hidden-sm hidden-md hidden-lg hidden-xl"></div>
            <div class="col-md-3">
              <div class="row">
                <div class="col-md-12 col-xs-6">
                  <button type="button" class="btn-lh btn-block btn-bamboo">Buat Kampanye</button>
                </div>
                <div class="col-md-12 col-xs-6">
                  <a href="{{ url('penggerak/lokasi') }}">
                    <button type="button" class="btn-lh btn-block btn-bamboo-outline">Lihat Lokasi</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="row padding-lg">
            <div class="col-md-2 col-xs-3">
              <img src="{{ asset('img/planting.png') }}" width="100%" alt="Wilayah Penanaman - LindungiHutan">
            </div>
            <div class="col-md-3 col-md-offset-1 hidden-xs">
              <strong>Tanggal Penanaman</strong><br>
              20 Desember 2020
              <div class="separator"></div>
              <strong>Lokasi</strong><br>
              Kampung Laut, Cilacap
            </div>
            <div class="col-md-3 hidden-xs">
              <strong>Kab/Kota</strong><br>
              Cilacap
              <div class="separator"></div>
              <strong>Jumlah</strong><br>
              2,936 Pohon dari<br>
              5 Kampanye Alam
            </div>
            <div class="col-xs-9 hidden-sm hidden-md hidden-lg hidden-xl">
              <strong>Kampung Laut Cilacap</strong><br>
              20 Desember 2020
            </div>
            <div class="separator-sm hidden-sm hidden-md hidden-lg hidden-xl"></div>
            <div class="col-md-3">
              <div class="row">
                <div class="col-md-12 col-xs-6">
                  <button type="button" class="btn-lh btn-block btn-bamboo">Buat Kampanye</button>
                </div>
                <div class="col-md-12 col-xs-6">
                  <a href="{{ url('penggerak/lokasi') }}">
                    <button type="button" class="btn-lh btn-block btn-bamboo-outline">Lihat Lokasi</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End planting area -->

      <img src="{{ asset('img/chat.png') }}" class="chat-icon" alt="Chat - HousePlantTree - LindungiHutan">
    </main>
@endsection

@section('js_source')
  <script src="{{ asset('js/jQuery-2.1.4.min.js') }}"></script>
  <script src="{{ asset('js/slick.min.js') }}"></script>
  <script src="{{ asset('js/imam.js') }}"></script>
@endsection
