@extends('layouts.app')

@section('title', 'Lokasi Kampanye - LindungiHutan')

@section('css_source')
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('body')
  <main class="container">
    <div class="separator"></div>

    <div class="row">
      <div class="col-sm-2 col-xs-3">
        <img src="{{ asset('img/profile.png') }}" class="img-rounded" alt="Kampung Laut, Cilacap - LindungiHutan" width="100%">
      </div>
      <div class="col-md-9 col-sm-8 col-xs-7">
        <h2 class="no-margin-top hidden-xs">
          <strong>Kampung Laut, Cilacap</strong>
        </h2>
        <h4 class="no-margin-top hidden-sm hidden-md hidden-lg hidden-xl">
          <strong>Kampung Laut, Cilacap</strong>
        </h4>
        <div>
          <i class="fa fa-map-marker clr-bamboo"></i>&nbsp;&nbsp;
          <small>Kabupaten / Kota : Cilacap</small>
        </div>
        <div>
          <i class="fa fa-tree clr-bamboo"></i>&nbsp;
          <small>Harga Pohon : Rp. 5.000</small>
        </div>
      </div>
      <div class="col-md-1 col-xs-2">
        <button type="button" class="btn btn-block btn-lg btn-bamboo hidden-xs">
          <i class="fa fa-map fa-2x"></i>
        </button>
        <button type="button" class="btn-lh btn-bamboo hidden-sm hidden-md hidden-lg hidden-xl">
          <i class="fa fa-map"></i>
        </button>
      </div>
    </div>
    <div class="row hidden-xs">
      <div class="col-sm-10 col-sm-offset-2">
        <h3><strong>Deskripsi</strong></h3>
        <p>Rusaknya mangrove membuat masyarakat paham betul bahwa bakau dan juga hutan mangrove sangat penting untuk menahan abrasi dan bahkan tsunami. Andreas Aji Wibowo memulai kegiatan konservasi lingkungan dan masyarakat juga diberikan pengertian dampak lingkungan dan ekonomi jika warga terus merusak bakau. Bukan hanya pantai yang terkikis gelombang, populasi biota laut akan turun dan akhirnya sumber pendapatan nelayan berkurang.</p>
      </div>
    </div>
    <div class="separator"></div>

    <div class="row">
      <div class="col-sm-7">
        Tampilkan
        <select>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
        entri
      </div>
      <div class="col-sm-5 hidden-xs">
        <div class="input-search">
          <i class="fa fa-search"></i>
          <input type="search" class="form-control" name="" placeholder="Cari Kampanye">
        </div>
      </div>
    </div>
    <div class="separator"></div>

    <div class="bg-aglonema rounded campaign-location-container">
      <div class="separator-md"></div>

      <div class="padding-lg">
        <!-- Sorter -->
        <div class="padding-h-lg flex">
          <div class="campaign-location button"></div>
          <div class="clickable campaign-location campaign">
            <strong>Kampanye</strong> <i class="fa fa-sort"></i>
          </div>
          <div class="clickable campaign-location status">
            <strong>Status</strong> <i class="fa fa-sort"></i>
          </div>
          <div class="clickable campaign-location tree">
            <strong>Jumlah Pohon</strong> <i class="fa fa-sort"></i>
          </div>
          <div class="clickable campaign-location emission">
            <strong>Emisi Berkurang</strong> <i class="fa fa-sort"></i>
          </div>
        </div>
        <!-- End Sorter -->

        <!-- List -->
        <div class="card">
          <div class="padding-lg flex">
            <div class="campaign-location button">
              <button type="button" class="btn-lh btn-block btn-bamboo-outline" data-toggle="modal" data-target="#modalCampaignMap">Lihat Update</button>
                <button type="button" class="btn-lh btn-block btn-bamboo" data-toggle="modal" data-target="#modalCampaign">Lihat Kampanye</button>
              <div class="separator-sm hidden-sm"></div>
            </div>
            <div class="campaign-location campaign"><strong>Pohon dari Sustainable Green Untuk Bumi</strong></div>
            <div class="campaign-location status">Belum Selesai</div>
            <div class="campaign-location tree">629 Pohon</div>
            <div class="campaign-location emission">200 KG</div>
          </div>
        </div>
      </div>
    </div>

    <img src="{{ asset('img/chat.png') }}" class="chat-icon" alt="Chat - HousePlantTree - LindungiHutan">
  </main>

  <div class="modal fade" id="modalCampaign" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><strong>Pohon Dari Sustainable Green Untuk Bumi</strong></h4>
        </div>
        <div class="modal-body">
          <div class="padding-lg" id="campaignLocation">
            <img src="{{ asset('img/campaign.png') }}" alt="Petani Penggerak - LindungiHutan">
            <img src="{{ asset('img/campaign.png') }}" alt="Petani Penggerak - LindungiHutan">
            <img src="{{ asset('img/campaign.png') }}" alt="Petani Penggerak - LindungiHutan">
          </div>

          <div class="row text-center" id="campaignDetail">
            <div class="col-xs-4">
              <img src="{{ asset('img/icons/treepng') }}" alt="Pohon Hidup - LindungiHutan">
              <p>
                Pohon Hidup<br>
                <strong>2000 Pohon</strong>
              </p>
            </div>
            <div class="col-xs-4">
              <img src="{{ asset('img/icons/dry-tree.png') }}" alt="Pohon Hidup - LindungiHutan">
              <p>
                Pohon Mati<br>
                <strong>10 Pohon</strong>
              </p>
            </div>
            <div class="col-xs-4">
              <img src="{{ asset('img/icons/tree_time.png') }}" alt="Pohon Hidup - LindungiHutan">
              <p>
                Umur Pohon<br>
                <strong>30 Hari</strong>
              </p>
            </div>
          </div>
          <div class="separator-sm"></div>

          <button type="button" class="btn btn-lh btn-bamboo center">Donasi Sekarang</button>
          <div class="separator-sm"></div>

          <p class="text-center">Bagikan ke:</p>
          <div style="display: flex; justify-content: center;">
            <a href="https://www.facebook.com/lindungihutandotcom/" target="_blank">
              <img src="{{ asset('img/icons/facebook.png') }}" alt="Facebook LindungiHutan.com">&nbsp;
            </a>
            <a href="https://twitter.com/lindungihutan/" target="_blank">
              <img src="{{ asset('img/icons/twitter.png') }}" alt="Twitter LindungiHutan.com">&nbsp;
            </a>
            <a href="https://www.linkedin.com/company/lindungihutan/" target="_blank">
              <img src="{{ asset('img/icons/linkedin.png') }}" alt="Linkedin LindungiHutan.com">&nbsp;
            </a>
            <a href="#">
              <img src="{{ asset('img/icons/whatsapp.png') }}" alt="WhatsApp LindungiHutan.com">
            </a>
          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div class="modal fade" id="modalCampaignMap" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><strong>Pohon Dari Sustainable Green Untuk Bumi</strong></h4>
        </div>
        <div class="modal-body">
          <iframe
            width="100%"
            height="400"
            frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed/v1/view?key=AIzaSyDw2iI4YUOnMhiZb4HxBQxwuyJ5IRqjTdM
            &center=-7.0000272,110.4109212&zoom=18&maptype=satellite" allowfullscreen>
          </iframe>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endsection

@section('js_source')
  <script src="{{ asset('js/jQuery-2.1.4.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/slick.min.js') }}"></script>
  <script src="{{ asset('js/imam.js') }}"></script>
@endsection
